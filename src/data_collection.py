import os
from pathlib import Path
from zipfile import ZipFile
from kaggle.api.kaggle_api_extended import KaggleApi

def authenticate_kaggle_api(api: KaggleApi, kaggle_json_path: str) -> None:
    api.authenticate()

def download_dataset(api: KaggleApi, dataset_name: str, destination_path: str) -> None:
    api.dataset_download_files(dataset_name, path=destination_path, unzip=True)

def cleanup_zip_file(zip_file_path: str) -> None:
    if os.path.exists(zip_file_path):
        os.remove(zip_file_path)

def main():
    api = KaggleApi()
    kaggle_json_path = '~/.kaggle/kaggle.json' #Kaggle API path
    
    # Authenticate Kaggle API
    authenticate_kaggle_api(api, kaggle_json_path)
    
    # Define the dataset name and destination path
    dataset_name = 'suraj520/customer-support-ticket-dataset'
    destination_path = '../data/raw/'
    
    # Download the dataset
    download_dataset(api, dataset_name, destination_path)
    
    # Clean up the zip file after extraction
    zip_file_path = os.path.join(destination_path, f'{dataset_name.split("/")[1]}.zip')
    cleanup_zip_file(zip_file_path)
    
    print(f'Dataset downloaded and extracted to {destination_path}')

if __name__ == "__main__":
    main()
