import pandas as pd
import numpy as np
import os
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Embedding, LSTM, Dense, Bidirectional, Dropout
from tensorflow.keras.callbacks import ModelCheckpoint
import matplotlib.pyplot as plt
import seaborn as sns

# Directory for results and predictions
RESULTS_DIR = '../assets/results_and_prediction'

#Directory for resutls and predictions 
CHECKPOINT_DIR = '../model_checkpoints/evaluation'

def create_directory(directory: str) -> None:
    if not os.path.exists(directory):
        os.makedirs(directory)

create_directory(RESULTS_DIR)

def load_data(file_path: str) -> pd.DataFrame:
    return pd.read_csv(file_path)

def preprocess_data(data: pd.DataFrame) -> tuple:
    data['text'] = data['Ticket Subject'].fillna('') + ' ' + data['Ticket Description'].fillna('')
    texts = data['text'].astype(str)
    labels = pd.get_dummies(data['Ticket Type']).values
    
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(texts)
    sequences = tokenizer.texts_to_sequences(texts)
    word_index = tokenizer.word_index
    max_length = max(len(seq) for seq in sequences)
    data_padded = pad_sequences(sequences, maxlen=max_length)
    
    X_train, X_test, y_train, y_test = train_test_split(data_padded, labels, test_size=0.2, random_state=42)
    
    return X_train, X_test, y_train, y_test, word_index, max_length

def build_model(word_index: dict, max_length: int, num_classes: int) -> tf.keras.Model:
    embedding_dim = 100
    
    model = Sequential([
        Embedding(len(word_index) + 1, embedding_dim, input_length=max_length),
        Bidirectional(LSTM(64)),
        Dense(64, activation='relu'),
        Dropout(0.5),
        Dense(num_classes, activation='softmax')
    ])
    
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    return model

def train_model(X_train: np.ndarray, y_train: np.ndarray, X_test: np.ndarray, y_test: np.ndarray, model: tf.keras.Model) -> tf.keras.callbacks.History:
    checkpoint_path = os.path.join(CHECKPOINT_DIR, 'model_checkpoint.keras')
    checkpoint_callback = ModelCheckpoint(filepath=checkpoint_path, save_best_only=True, monitor='val_loss', mode='min')
    
    history = model.fit(X_train, y_train, epochs=50, batch_size=32, validation_data=(X_test, y_test), callbacks=[checkpoint_callback])
    
    plot_accuracy(history)
    plot_loss(history)
    plot_performance_table(history)
    
    return history

def evaluate_model(model: tf.keras.Model, X_test: np.ndarray, y_test: np.ndarray) -> tuple:
    loss, accuracy = model.evaluate(X_test, y_test)
    predictions = model.predict(X_test)
    return loss, accuracy, predictions

def plot_accuracy(history: tf.keras.callbacks.History) -> None:
    plt.figure(figsize=(8, 6))
    plt.plot(history.history['accuracy'], label='Training Accuracy')
    plt.plot(history.history['val_accuracy'], label='Validation Accuracy')
    plt.title('Model Accuracy')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(os.path.join(RESULTS_DIR, 'model_accuracy.png'))
    plt.close()

def plot_loss(history: tf.keras.callbacks.History) -> None:
    plt.figure(figsize=(8, 6))
    plt.plot(history.history['loss'], label='Training Loss')
    plt.plot(history.history['val_loss'], label='Validation Loss')
    plt.title('Model Loss')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.legend()
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(os.path.join(RESULTS_DIR, 'model_loss.png'))
    plt.close()

def plot_performance_table(history: tf.keras.callbacks.History) -> None:
    metrics_df = pd.DataFrame({
        'Epoch': range(len(history.history['accuracy'])),
        'Training Accuracy (%)': [acc * 100 for acc in history.history['accuracy']],
        'Validation Accuracy (%)': [val_acc * 100 for val_acc in history.history['val_accuracy']],
        'Training Loss': history.history['loss'],
        'Validation Loss': history.history['val_loss']
    })

    # Filter the last 5 epochs
    metrics_df = metrics_df.tail(5)

    # Plot as table
    fig, ax = plt.subplots(figsize=(8, 4))
    ax.axis('tight')
    ax.axis('off')
    ax.table(cellText=metrics_df.values, colLabels=metrics_df.columns, cellLoc='center', loc='center')

    plt.tight_layout()
    plt.savefig(os.path.join(RESULTS_DIR, 'performance_table.png'))
    plt.close()

def plot_predictions(predictions: np.ndarray, y_test: np.ndarray) -> None:
    y_test_labels = np.argmax(y_test, axis=1)
    predictions_labels = np.argmax(predictions, axis=1)
    
    plt.figure(figsize=(10, 8))
    sns.heatmap(pd.crosstab(y_test_labels, predictions_labels, rownames=['Actual'], colnames=['Predicted']), annot=True, fmt='d', cmap='Blues')
    plt.title('Confusion Matrix')
    plt.tight_layout()
    plt.savefig(os.path.join(RESULTS_DIR, 'confusion_matrix.png'))
    plt.close()

def main(file_path: str) -> None:
    data = load_data(file_path)
    X_train, X_test, y_train, y_test, word_index, max_length = preprocess_data(data)
    num_classes = len(pd.get_dummies(data['Ticket Type']).columns)
    model = build_model(word_index, max_length, num_classes)
    history = train_model(X_train, y_train, X_test, y_test, model)
    loss, accuracy, predictions = evaluate_model(model, X_test, y_test)
    plot_predictions(predictions, y_test)

if __name__ == '__main__':
    file_path = '../data/processed/processed_customer_support_tickets.csv'
    main(file_path)
