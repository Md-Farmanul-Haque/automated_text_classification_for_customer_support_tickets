import os
import pandas as pd
import matplotlib.pyplot as plt
from wordcloud import WordCloud
from collections import Counter

# Directory to store generated visuals
OUTPUT_DIR = '../assets/data_insight'
if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)

# Function to load data
def load_data(file_path):
    return pd.read_csv(file_path)

# Function to generate wordclouds
def generate_wordclouds(data):
    subject_text = ' '.join(data['Ticket Subject'].astype(str))
    description_text = ' '.join(data['Ticket Description'].astype(str))
    
    # Generate wordcloud for ticket subjects
    wordcloud_subject = WordCloud(width=800, height=400, background_color='white').generate(subject_text)
    plt.figure(figsize=(10, 5))
    plt.imshow(wordcloud_subject, interpolation='bilinear')
    plt.title('Wordcloud of Ticket Subjects')
    plt.axis('off')
    plt.savefig(os.path.join(OUTPUT_DIR, 'wordcloud_ticket_subjects.png'))
    plt.close()
    
    # Generate wordcloud for ticket descriptions
    wordcloud_description = WordCloud(width=800, height=400, background_color='white').generate(description_text)
    plt.figure(figsize=(10, 5))
    plt.imshow(wordcloud_description, interpolation='bilinear')
    plt.title('Wordcloud of Ticket Descriptions')
    plt.axis('off')
    plt.savefig(os.path.join(OUTPUT_DIR, 'wordcloud_ticket_descriptions.png'))
    plt.close()

# Function to plot ticket channel distribution
def plot_channel_distribution(data):
    channel_counts = data['Ticket Channel'].value_counts()
    plt.figure(figsize=(8, 6))
    channel_counts.plot(kind='bar', color='skyblue')
    plt.title('Ticket Channel Distribution')
    plt.xlabel('Channel')
    plt.ylabel('Number of Tickets')
    plt.grid(True, axis='y')
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()
    plt.savefig(os.path.join(OUTPUT_DIR, 'ticket_channel_distribution.png'))
    plt.close()

# Function to plot ticket status distribution
def plot_status_distribution(data):
    status_counts = data['Ticket Status'].value_counts()
    plt.figure(figsize=(8, 8))
    plt.pie(status_counts, labels=status_counts.index, autopct='%1.1f%%', startangle=140, colors=['lightgreen', 'lightcoral', 'lightskyblue'])
    plt.title('Ticket Status Distribution')
    plt.axis('equal')
    plt.tight_layout()
    plt.savefig(os.path.join(OUTPUT_DIR, 'ticket_status_distribution.png'))
    plt.close()

# Main function to orchestrate visualization generation
def generate_visualizations(file_path):
    # Load data
    data = load_data(file_path)
    
    # Generate wordclouds
    generate_wordclouds(data)
    
    # Plot ticket channel distribution
    plot_channel_distribution(data)
    
    # Plot ticket status distribution
    plot_status_distribution(data)

# Entry point
if __name__ == '__main__':
    file_path = '../data/processed/processed_customer_support_tickets.csv'
    generate_visualizations(file_path)
    print("Visualizations generated successfully.")
