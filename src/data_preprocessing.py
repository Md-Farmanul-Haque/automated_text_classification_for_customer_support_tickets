import pandas as pd
import os
from typing import Tuple

def read_data(file_path: str) -> pd.DataFrame:
    return pd.read_csv(file_path)

def clean_text(text: str) -> str:
    if pd.isna(text):
        return ""
    # Add more cleaning steps as needed
    cleaned_text = text.replace("{product_purchased}", "product")
    return cleaned_text

def preprocess_data(df: pd.DataFrame) -> pd.DataFrame:
    df['Ticket Description'] = df['Ticket Description'].apply(clean_text)
    df['Resolution'] = df['Resolution'].fillna('No resolution provided')
    df['First Response Time'] = pd.to_datetime(df['First Response Time'])
    df['Time to Resolution'] = pd.to_datetime(df['Time to Resolution'])
    
    return df

def save_data(df: pd.DataFrame, file_path: str) -> None:
    df.to_csv(file_path, index=False)

def main() -> None:
    raw_data_path = '../data/raw/customer_support_tickets.csv'
    processed_data_path = '../data/processed/processed_customer_support_tickets.csv'
    
    # Ensure the processed data directory exists
    os.makedirs(os.path.dirname(processed_data_path), exist_ok=True)
    
    # Read, preprocess, and save the data
    data = read_data(raw_data_path)
    processed_data = preprocess_data(data)
    save_data(processed_data, processed_data_path)
    
    print(f"Data has been processed and saved to {processed_data_path}")

if __name__ == "__main__":
    main()
